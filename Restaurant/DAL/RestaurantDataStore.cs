﻿using System;
using Restaurant.EF;
using Restaurant.EF.Entities;
using Restaurant.DAL.Repositories;

namespace Restaurant.DAL
{
    public class RestaurantDataStore : IDisposable
    {
        private RestaurantDbContext _ctx;

        public EfGenericRepository<Product> Products { get; set; }
        public EfGenericRepository<Unit> UnitSet { get; set; }
        public EfGenericRepository<ProductOrder> PrductOrderLog { get; set; }
        public EfGenericRepository<Ingredient> Ingredients { get; set; }
        public EfGenericRepository<Dish> Dishes { get; set; }
        public EfGenericRepository<DishOrder> DishOrder { get; set; }
        public EfGenericRepository<StorageItem> Storage { get; set; }

        public RestaurantDataStore(string connectionString)
        {
            _ctx = new RestaurantDbContext(connectionString);

            Products = new EfGenericRepository<Product>(_ctx);
            UnitSet  = new EfGenericRepository<Unit>(_ctx);
            PrductOrderLog = new EfGenericRepository<ProductOrder>(_ctx);
            Ingredients = new EfGenericRepository<Ingredient>(_ctx);
            Dishes = new EfGenericRepository<Dish>(_ctx);
            DishOrder = new EfGenericRepository<DishOrder>(_ctx);
            Storage = new EfGenericRepository<StorageItem>(_ctx);
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
