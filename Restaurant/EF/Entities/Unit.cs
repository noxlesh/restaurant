﻿using System.ComponentModel.DataAnnotations;

namespace Restaurant.EF.Entities
{
    public class Unit
    {
        public int Id { get; set; }
        [Required,MaxLength(10)]
        public string Name { get; set; }
    }
}