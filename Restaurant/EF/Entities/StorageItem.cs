﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Restaurant.EF.Entities
{
    [Table("Storage")]
    public class StorageItem
    {
        public int Id { get; set; }
        [Required]
        public virtual Product Product { get; set; }
        [Required]
        public float Quantity { get; set; }
    }
}
