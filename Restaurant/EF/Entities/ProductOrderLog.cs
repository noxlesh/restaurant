﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurant.EF.Entities
{
    [Table("ProductOrderLog")]
    public class ProductOrder
    {
        public int Id { get; set; }
        public DateTime OrderedIn { get; set; }
        [Required]
        public virtual Product Product { get; set; }
        [Required]
        public float Quantity { get; set; }

        public ProductOrder()
        {
            OrderedIn = DateTime.Now;
        }
    }
}