﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurant.EF.Entities
{
    [Table("DishOrderLog")]
    public class DishOrder
    {
        public int Id { get; set; }
        public DateTime OrderedIn { get; set; }
        [Required]
        public virtual Dish Dish { get; set; }
        [Required]
        public decimal SellPrice { get; set; }
        public DishOrder()
        {
            OrderedIn = DateTime.Now;
        }
    }
}