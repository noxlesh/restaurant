﻿using System.Data.Entity;
using Restaurant.EF.Entities;

namespace Restaurant.EF
{
    public class RestaurantDbContext : DbContext
    {
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<StorageItem> Storage { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Unit> UnitTypes { get; set; }
        public DbSet<DishOrder> DishOrderLog { get; set; }
        public DbSet<ProductOrder> ProductOrderLog { get; set; }
        
        
        public RestaurantDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            Database.SetInitializer(new RestaurantDbInitializer());
        }
    }
}