﻿using System.Data.Entity;
using Restaurant.EF.Entities;

namespace Restaurant.EF
{
    public class RestaurantDbInitializer : DropCreateDatabaseAlways<RestaurantDbContext>
    {
        protected override void Seed(RestaurantDbContext context)
        {
            //Unit pcsUnit =  new Unit { Name = "pcs." };
            //Unit litreUnit = new Unit { Name = "L." };
            //Unit kilogrammUnit = new Unit { Name = "kg."};
            //context.UnitTypes.Add(pcsUnit);
            //context.UnitTypes.Add(litreUnit);
            //context.UnitTypes.Add(kilogrammUnit);
            //context.SaveChanges();

            //Product apple = new Product {Name = "Apple", Unit = kilogrammUnit, Price = 5.52M };
            //Product milk = new Product {Name = "Milk", Unit = litreUnit, Price = 1.24M };
            //Product shugar = new Product {Name = "Shugar", Unit = kilogrammUnit, Price = 2.3M };
            //Product flour = new Product {Name = "Flour", Unit = kilogrammUnit, Price = 8.3M };
            //Product soda = new Product {Name = "Soda", Unit = kilogrammUnit, Price = 7.38M };
            //context.Products.Add(apple);
            //context.Products.Add(milk);
            //context.Products.Add(shugar);
            //context.Products.Add(flour);
            //context.Products.Add(soda);
            //context.SaveChanges();

            //StorageItem si1 = new StorageItem { Product = apple, Quantity = 20F};
            //StorageItem si2 = new StorageItem { Product = milk , Quantity = 15.3F};
            //context.Storage.Add(si1);
            //context.Storage.Add(si2);
            //context.SaveChanges();

            //Dish applePie = new Dish { Name = "ApplePie", RecipeDescription = "Mix all the ingredients and bake :)" };
            //context.Dishes.Add(applePie);
            //context.SaveChanges();

            //Ingredient applePieIng1 = new Ingredient{ Dish = applePie, Product = apple, Quantity = 3F};
            //Ingredient applePieIng2 = new Ingredient{ Dish = applePie, Product = milk, Quantity = 0.3F};
            //Ingredient applePieIng3 = new Ingredient{ Dish = applePie, Product = shugar, Quantity = 0.1F};
            //Ingredient applePieIng4 = new Ingredient{ Dish = applePie, Product = flour, Quantity = 0.5F};
            //Ingredient applePieIng5 = new Ingredient{ Dish = applePie, Product = soda, Quantity = 0.005F};
            //context.Ingredients.Add(applePieIng1);
            //context.Ingredients.Add(applePieIng2);
            //context.Ingredients.Add(applePieIng3);
            //context.Ingredients.Add(applePieIng4);
            //context.Ingredients.Add(applePieIng5);
            //context.SaveChanges();

            //DishOrder do1 = new DishOrder{ Dish = applePie, SellPrice = 12.2M };
            //DishOrder do2 = new DishOrder { Dish = applePie, SellPrice = 12.2M };
            //context.DishOrderLog.Add(do1);
            //context.DishOrderLog.Add(do2);
            //context.SaveChanges();

            //ProductOrder po1 = new ProductOrder{ Product = apple, Quantity = 100F};
            //ProductOrder po2 = new ProductOrder{ Product = milk, Quantity = 50F};
            //ProductOrder po3 = new ProductOrder{ Product = shugar, Quantity = 20F};
            //ProductOrder po4 = new ProductOrder{ Product = flour, Quantity = 50F};
            //ProductOrder po5 = new ProductOrder{ Product = soda, Quantity = 1F};
            //context.ProductOrderLog.Add(po1);
            //context.ProductOrderLog.Add(po2);
            //context.ProductOrderLog.Add(po3);
            //context.ProductOrderLog.Add(po4);
            //context.ProductOrderLog.Add(po5);
            //context.SaveChanges();

        }
    }
}