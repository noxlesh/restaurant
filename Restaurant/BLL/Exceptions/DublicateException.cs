﻿using System;

namespace Restaurant.BLL.Exceptions
{
    public class DublicateException : Exception
    {
        public DublicateException(string message) : base(message)
        {
        }
    }
}