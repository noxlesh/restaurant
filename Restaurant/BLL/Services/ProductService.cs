﻿using System.Collections.Generic;
using System.Linq;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Exceptions;
using Restaurant.BLL.Interfaces;
using Restaurant.DAL;
using Restaurant.EF.Entities;

namespace Restaurant.BLL.Services
{

    public class ProductService : IProductService
    {
        private RestaurantDataStore _dataStore;

        public ProductService(RestaurantDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public void Add(ProductDTO product)
        {
            string productName = product.Name;
            if (_dataStore.Products.Get(p => p.Name == productName).Any())
            {
                throw new DublicateException($"Product with '{productName}' name already exists!");
            }

            Unit efUnit = _dataStore.UnitSet.FindById(product.UnitId);
            Product efProduct = new Product {Name = productName, Unit = efUnit};
            _dataStore.Products.Create(efProduct);
        }

        public ICollection<ProductDTO> All()
        {
            List<Product> efProductList = _dataStore.Products.Get().ToList();
            ICollection<ProductDTO> productList =
                efProductList.Select(p => new ProductDTO {Id = p.Id, Name = p.Name, UnitId = p.Unit.Id}).ToList();
            return productList;
        }

        public void Dispose()
        {
            _dataStore.Dispose();
        }
    }
}