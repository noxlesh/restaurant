﻿using System.Collections.Generic;
using System.Linq;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Exceptions;
using Restaurant.BLL.Interfaces;
using Restaurant.DAL;
using Restaurant.EF.Entities;

namespace Restaurant.BLL.Services
{
    public class StorageItemService : IStorageItemService
    {
        private RestaurantDataStore _dataStore;

        public StorageItemService(RestaurantDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public void Add(StorageItemDTO storageItem)
        {
            int productId = storageItem.ProductId;
            if (_dataStore.Storage.Get(i => i.Product.Id == productId).Any())
            {
                throw new DublicateException($"StorageItem with '{productId}' productId already exists!");
            }

            Product efProduct = _dataStore.Products.FindById(productId);
            StorageItem efStorageItem = 
                new StorageItem{ Product = efProduct, Quantity = storageItem.Quantity};
            _dataStore.Storage.Create(efStorageItem);
        }

        public ICollection<StorageItemDTO> All()
        {
            List<StorageItem> efStorageItemList = _dataStore.Storage.Get().ToList();
            ICollection<StorageItemDTO> storageItemList =
                efStorageItemList.Select(p => new StorageItemDTO {ProductId = p.Product.Id,
                    Quantity = p.Quantity}).ToList();
            return storageItemList;
        }

        public void Dispose()
        {
            _dataStore.Dispose();
        }
    }
}