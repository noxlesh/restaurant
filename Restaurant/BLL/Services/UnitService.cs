﻿using System;
using System.Collections.Generic;
using System.Linq;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Exceptions;
using Restaurant.BLL.Interfaces;
using Restaurant.DAL;
using Restaurant.EF.Entities;

namespace Restaurant.BLL.Services
{
    public class UnitService : IUnitService
    {
        private RestaurantDataStore _dataStore;

        public UnitService(RestaurantDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public void Add(UnitDTO unit)
        {
            string unitName = unit.Name;
            if (_dataStore.UnitSet.Get(u => u.Name == unitName).Any()) // an unit name must be unique
            {
                throw new DublicateException($"Unit with '{unitName}' name already exists!");
            }

            Unit efUnit = new Unit { Name = unitName };
            _dataStore.UnitSet.Create(efUnit);
        }

        public ICollection<UnitDTO> All()
        {
            List<Unit> efUnitList =  _dataStore.UnitSet.Get().ToList();
            ICollection<UnitDTO> unitList =
                efUnitList.Select(u => new UnitDTO {Id = u.Id, Name = u.Name}).ToList();
            return unitList;
        }

        public void Dispose()
        {
            _dataStore.Dispose();
        }
    }
}