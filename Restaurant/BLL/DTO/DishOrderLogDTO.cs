﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Restaurant.EF.Entities;

namespace Restaurant.BLL.DTO
{
    public class DishOrderLogDTO
    {
        public int Id { get; set; }
        public DateTime? OrderedIn { get; set; }
        public int DishId { get; set; }
        public decimal SellPrice { get; set; }
    }
}
