﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant.BLL.DTO
{
    public class ProductOrderLogDTO
    {
        public int Id { get; set; }
        public DateTime? OrderedIn { get; set; }
        public int ProductId { get; set; }
        public float Quantity { get; set; }
    }
}
