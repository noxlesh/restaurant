﻿namespace Restaurant.BLL.Interfaces
{
    public interface IService
    {
        void Dispose();
    }
}