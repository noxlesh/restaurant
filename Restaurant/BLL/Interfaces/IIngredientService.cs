﻿using System.Collections.Generic;
using Restaurant.BLL.DTO;

namespace Restaurant.BLL.Interfaces
{
    public interface IIngredientService : IService
    {
        void Add(IngredientDTO ingredient);
        ICollection<IngredientDTO> All();
    }
}