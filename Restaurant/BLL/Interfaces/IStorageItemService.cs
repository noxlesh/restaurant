﻿using System.Collections.Generic;
using Restaurant.BLL.DTO;

namespace Restaurant.BLL.Interfaces
{
    public interface IStorageItemService : IService
    {
        void Add(StorageItemDTO storageItem);
        ICollection<StorageItemDTO> All();
    }
}