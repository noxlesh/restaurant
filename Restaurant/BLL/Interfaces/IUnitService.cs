﻿using System.Collections.Generic;
using Restaurant.BLL.DTO;

namespace Restaurant.BLL.Interfaces
{
    public interface IUnitService : IService
    {
        void Add(UnitDTO unit);
        ICollection<UnitDTO> All();
    }
}