﻿using System.Collections.Generic;
using Restaurant.BLL.DTO;

namespace Restaurant.BLL.Interfaces
{
    public interface IDishService : IService
    {
        void Add(DishDTO dish);
        ICollection<DishDTO> All();
    }
}