﻿using System.Collections.Generic;
using Restaurant.BLL.DTO;

namespace Restaurant.BLL.Interfaces
{
    public interface IDishOrderServiceLog : IService
    {
        void Add(DishOrderLogDTO dishOrder);
        ICollection<DishOrderLogDTO> All();
    }
}