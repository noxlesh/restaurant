﻿using Restaurant.BLL.DTO;
using System.Collections.Generic;

namespace Restaurant.BLL.Interfaces
{
    public interface IProductOrderServiceLog : IService
    {
        void Add(ProductOrderLogDTO productOrder);
        ICollection<ProductOrderLogDTO> All();
    }
}