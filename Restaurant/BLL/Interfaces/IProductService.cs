﻿using System.Collections.Generic;
using Restaurant.BLL.DTO;

namespace Restaurant.BLL.Interfaces
{
    public interface IProductService : IService
    {
        void Add(ProductDTO product);
        ICollection<ProductDTO> All();
    }
}