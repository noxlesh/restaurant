﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.DAL;
using Restaurant.DAL.Repositories;
using Restaurant.EF.Entities;

namespace UnitTests
{
    [TestClass]
    public class UnitGenericRepositoryDataValidation
    {
        [TestMethod]
        [ExpectedException(typeof(EntityValidationException))]
        public void TestOnCreateEntityValidation()
        {
            RestaurantDataStore rds = new RestaurantDataStore("Conn");
            Unit u = rds.UnitSet.FindById(1);
            Product p = new Product {Name = "TestTestTestTestTestTestTestTestTest", Unit = u};
            //assert
            rds.Products.Create(p);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityValidationException))]
        public void TestOnUpdateEntityValidation()
        {
            RestaurantDataStore rds = new RestaurantDataStore("Conn");
            Unit u = rds.UnitSet.FindById(1);
            Product p = new Product { Name = "TestTestTestTestTestTestTestTestTest", Unit = u };
            rds.Products.Create(p);
            Product p1 = rds.Products.FindById(1);
            p1.Name = "TestTestTestTestTestTestTestTestTest";
            //assert
            rds.Products.Update(p);
        }
    }
}
