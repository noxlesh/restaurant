﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Exceptions;
using Restaurant.BLL.Services;
using Restaurant.DAL;
using Restaurant.EF.Entities;

namespace UnitTests.BLL
{
    [TestClass]
    public class UnitServiceTests
    {
        [TestMethod]
        public void TestAddNewUnit()
        {
            string name = "li.";
            RestaurantDataStore dataStore = new RestaurantDataStore("Conn");
            UnitDTO unitDto = new UnitDTO { Name = name };
            UnitService unitService = new UnitService(dataStore);
            unitService.Add(unitDto);
            Unit unit = dataStore.UnitSet.Get(u => u.Name == name).ToList()[0];
            // assert
            Assert.IsNotNull(unit);
            unitService.Dispose();
        }

        [TestMethod]
        [ExpectedException(typeof(DublicateException))]
        public void TestAddDublicateUnit()
        {
            string name = "L";
            RestaurantDataStore dataStore = new RestaurantDataStore("Conn");
            UnitDTO unitDto1 = new UnitDTO { Name = name };
            UnitDTO unitDto2 = new UnitDTO { Name = name };
            UnitService unitService = new UnitService(dataStore);
            unitService.Add(unitDto1);
            // assert
            unitService.Add(unitDto2);
            unitService.Dispose();
        }

        [TestMethod]
        public void TestGetAllUnits()
        {
            List<string> nameList = new List<string> { "L", "Kg"};
            RestaurantDataStore dataStore = new RestaurantDataStore("Conn");
            UnitDTO unitDto1 = new UnitDTO { Name = nameList[0] };
            UnitDTO unitDto2 = new UnitDTO { Name = nameList[1] };
            UnitService unitService = new UnitService(dataStore);
            unitService.Add(unitDto1);
            unitService.Add(unitDto2);

            List<UnitDTO> unitList = unitService.All().ToList();

            Assert.AreEqual(nameList[0], unitList[0].Name);
            Assert.AreEqual(nameList[1], unitList[1].Name);
        }
    }
}
