﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Exceptions;
using Restaurant.BLL.Services;
using Restaurant.DAL;
using Restaurant.EF.Entities;

namespace UnitTests
{
    [TestClass]
    public class StorageItemTests
    {
        private RestaurantDataStore _dataStore;

        [TestInitialize]
        public void TestInitializeDataStore()
        {
            _dataStore = new RestaurantDataStore("Conn");
            UnitDTO unitDto1 = new UnitDTO { Name = "L" };
            UnitDTO unitDto2 = new UnitDTO { Name = "Kg" };
            UnitDTO unitDto3 = new UnitDTO { Name = "Pc" }; // piece
            UnitService unitService = new UnitService(_dataStore);
            unitService.Add(unitDto1);
            unitService.Add(unitDto2);
            unitService.Add(unitDto3);
            ProductDTO productDto1 = new ProductDTO { Name = "Egg", Price = 1.5M, UnitId = 3 }; // Pc
            ProductDTO productDto2 = new ProductDTO { Name = "Shugar", Price = 1.6M, UnitId = 2 }; // Kg
            ProductService productService = new ProductService(_dataStore);
            productService.Add(productDto1);
            productService.Add(productDto2);
        }


        [TestMethod]
        public void TestAddNewStorageItem()
        {
            float quantity = 10F;
            int productId = 1; // Egg
            StorageItemDTO storageItemDto = new StorageItemDTO { ProductId = productId, Quantity = quantity};
            StorageItemService storageItemService = new StorageItemService(_dataStore);
            storageItemService.Add(storageItemDto);
            StorageItem storageItem = _dataStore.Storage.FindById(1);
            
            Assert.IsNotNull(storageItem);
            Assert.AreEqual(productId, storageItem.Product.Id);
            Assert.AreEqual(quantity, storageItem.Quantity);
            storageItemService.Dispose();
        }

        [TestMethod]
        [ExpectedException(typeof(DublicateException))]
        public void TestAddDublicateProductId()
        {
            float quantity = 10F;
            int productId1 = 1; // Egg 
            StorageItemDTO storageItemDto1 = new StorageItemDTO { ProductId = productId1, Quantity = quantity };
            StorageItemDTO storageItemDto2 = new StorageItemDTO { ProductId = productId1, Quantity = quantity };
            StorageItemService storageItemService = new StorageItemService(_dataStore);
            storageItemService.Add(storageItemDto1);
            // assert exception
            storageItemService.Add(storageItemDto2);
            storageItemService.Dispose();
        }

        [TestMethod]
        public void TestGetAllStorageItems()
        {
            List<float> quantityList = new List<float> { 10F, 1.5f};
            List<int> productIdList= new List<int> { 1 , 2}; // Egg,  Shugar
            StorageItemDTO storageItemDto1 = new StorageItemDTO { ProductId = productIdList[0], Quantity = quantityList[0] };
            StorageItemDTO storageItemDto2 = new StorageItemDTO { ProductId = productIdList[1], Quantity = quantityList[1] };
            StorageItemService storageItemService = new StorageItemService(_dataStore);
            storageItemService.Add(storageItemDto1);
            storageItemService.Add(storageItemDto2);
            List<StorageItemDTO> storageItemList = storageItemService.All().ToList();

            Assert.AreEqual(productIdList[0], storageItemList[0].ProductId);
            Assert.AreEqual(productIdList[1], storageItemList[1].ProductId);
            Assert.AreEqual(quantityList[0], storageItemList[0].Quantity);
            Assert.AreEqual(quantityList[1], storageItemList[1].Quantity);
            storageItemService.Dispose();

        }
    }
}
