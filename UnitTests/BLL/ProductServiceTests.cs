﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Exceptions;
using Restaurant.BLL.Services;
using Restaurant.DAL;
using Restaurant.EF.Entities;

namespace UnitTests
{
    [TestClass]
    public class ProductServiceTests
    {
        private RestaurantDataStore _dataStore;

        [TestInitialize]
        public void TestInitializeDataStore()
        {
            _dataStore = new RestaurantDataStore("Conn");
            UnitDTO unitDto1 = new UnitDTO { Name = "L" };
            UnitDTO unitDto2 = new UnitDTO { Name = "Kg" };
            UnitDTO unitDto3 = new UnitDTO { Name = "Pc" }; // piece
            UnitService unitService = new UnitService(_dataStore);
            
            unitService.Add(unitDto1);
            unitService.Add(unitDto2);
            unitService.Add(unitDto3);
        }

        [TestMethod]
        public void TestAddNewProduct()
        {
            int unitId = 2; // "Kg' 
            string name = "Apple";
            ProductDTO productDto = new ProductDTO {Name = name, Price = 1.5M, UnitId = unitId};
            ProductService productService = new ProductService(_dataStore);
            productService.Add(productDto);
            Product product = _dataStore.Products.FindById(1);

            Assert.IsNotNull(product);
            Assert.AreEqual(unitId, product.Unit.Id);
            Assert.AreEqual(name, product.Name);
            productService.Dispose();
        }

        [TestMethod]
        [ExpectedException(typeof(DublicateException))]
        public void TestAddDublicateProduct()
        {
            int unitId = 3; // "Pc' 
            string name = "Apple";
            ProductDTO productDto1 = new ProductDTO {Name = name, Price = 1.5M, UnitId = unitId};
            ProductDTO productDto2 = new ProductDTO {Name = name, Price = 1.6M, UnitId = unitId};
            ProductService productService = new ProductService(_dataStore);
            productService.Add(productDto1);
            // assert
            productService.Add(productDto2);
            productService.Dispose();
        }

        [TestMethod]
        public void TestGetAllProducts()
        {
            List<string> nameList = new List<string> { "Apple", "Milk" };
            List<int> unitIdList = new List<int> { 3, 1 };
            ProductDTO productDto1 = new ProductDTO { Name = nameList[0], Price = 1.5M, UnitId = unitIdList[0] };
            ProductDTO productDto2 = new ProductDTO { Name = nameList[1], Price = 1.6M, UnitId = unitIdList[1] };
            ProductService productService = new ProductService(_dataStore);
            productService.Add(productDto1);
            productService.Add(productDto2);
            List<ProductDTO> productList = productService.All().ToList();

            Assert.AreEqual(nameList[0], productList[0].Name);
            Assert.AreEqual(nameList[1], productList[1].Name);
            Assert.AreEqual(unitIdList[0], productList[0].UnitId);
            Assert.AreEqual(unitIdList[1], productList[1].UnitId);
            productService.Dispose();
        }
    }
}
